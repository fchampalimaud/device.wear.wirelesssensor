## What software do I need to view or open the files?

The code is developed on the [Keil](http://www.keil.com/)'s IDE. The current version being used is the compiler C51 V9.53.0.0 that can be downloaded [here](https://bitbucket.org/fchampalimaud/device.wear.basestation.wirelessrf1receiver/downloads/C51-9.53.0.0.exe).

## Licensing ##

Each subdirectory will contain a license or, possibly, a set of licenses if it involves both hardware and software.

## How do I get set up? ###

### Compile the Code

1. Install Keil IDE & compiler.
2. Additional Nordic Semiconductors software is needed to be able to compile the code (and further programming of the microcontroller). Install the [nRFgo Studio](https://bitbucket.org/fchampalimaud/device.wear.basestation.wirelessrf1receiver/downloads/nRFgo-Studio-1.18.0.msi), the [nRFProbe](https://bitbucket.org/fchampalimaud/device.wear.basestation.wirelessrf1receiver/downloads/nRF-Probe-2.0.0.7252.exe) and the [nRFgo SDK](https://bitbucket.org/fchampalimaud/device.wear.basestation.wirelessrf1receiver/downloads/nRFgo-SDK-2.3.0.exe).
3. Open the project named **wear.uvproj** on the folder **Firmware\WirelessSensor**.
4. Go to **Project > Options for Target > Target** and update the **Xtal (MHz):** to **16**. Save this by going to **File > Save All**.
5. To compile, use the command **Project > Rebuild all target files**.
6. The output file (file **WirelessSensor.hex**) can be found on the folder **Firmware\WirelessSensor**.

### Keil License

To be able to compile the code, a Keil license is mandatory.