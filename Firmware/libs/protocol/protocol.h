#ifndef _PROTOCOL_H_
#define _PROTOCOL_H_

/************************************************************************/
/* Protocol                                                             */
/************************************************************************/
/*
 == WEAR -> INTERFACE ==
 (data)
 0x00(1),id_LSByte(1),counter(1),tx fails(1),data(6,12,18)
 0x01(1),id_LSByte(1),counter(1),129,data(6,12,18)
 0x02(1),id_LSByte(1),counter(1),rx pwr rx receiver > -64 dBm(1),data(6,12,18)
 0x03(1),id_LSByte(1),counter(1),temperature(1),data(6,12,18)
 0x04(1),id_LSByte(1),counter(1),freq(1),data(6,12,18)
 0x05(1),id_LSByte(1),counter(1),tx pwr(1),data(6,12,18)
 0x06(1),id_LSByte(1),counter(1),LED(1),data(6,12,18)
 0x07(1),id_LSByte(1),counter(1),range xyz(1),data(6,12,18)
 0x08(1),id_LSByte(1),counter(1),range gyr(1),data(6,12,18)
 0x09(1),id_LSByte(1),counter(1),range mag(1),data(6,12,18)
 0x0A(1),id_LSByte(1),counter(1),ch mask(1),data(6,12,18)
 0x0B(1),id_LSByte(1),counter(1),battery_filtered(1),data(6,12,18)
 0x0C(1),id_LSByte(1),counter(1),stim_counter(1),data(6,12,18)
 0x0D(1),id_LSByte(1),counter(1),battery_raw(1),data(6,12,18)
 0x0E(1),id_LSByte(1),counter(1),wear_fw_version_major(1),data(6,12,18)
 0x0F(1),id_LSByte(1),counter(1),wear_fw_version_minor(1),data(6,12,18)
 0x10(1),id_LSByte(1),counter(1),wear_hw_version_major(1),data(6,12,18)
 0x11(1),id_LSByte(1),counter(1),wear_hw_version_minor(1),data(6,12,18)
 0x12(1),id_LSByte(1),counter(1),receiver_fw_version_major(1),data(6,12,18)
 0x13(1),id_LSByte(1),counter(1),receiver_fw_version_minor(1),data(6,12,18)
 0x14(1),id_LSByte(1),counter(1),receiver_hw_version_major(1),data(6,12,18)
 0x15(1),id_LSByte(1),counter(1),receiver_hw_version_minor(1),data(6,12,18)

 
 Note 1: All unsigned expect temperature
 Note 2: temperature �C = (temperature * 256) / 340 + 35
 Note 3: counter goes from 0 to 255
 
 == INTERFACE -> USB ==
 "w>:"(data)
 "aaaaa"(stimulation acknowledge)
 
 == UDP -> LABVIEW ==
 "r<:"(commands)
 
 == USB -> INTERFACE -> WEAR ==
 (wake up: 8 bytes)
 [0x60(1)][freq(1)][LED_txpwr(1)][range xyz(1)][range gyr(1)][range mag(1)][id_chmask(1)][[id_LSByte(1)]
		[freq]						- [7]   1: base station use LED, 0: turn always the LED off
												-- Note: This bit is cleared before sent to slave devices
											- [6:5] data rate: 0 (2 Mbps), 1 (1 Mbps), 2 (250Kbps) (not used on wired device)
											- [4:0] frequency: 0 (50 Hz), 1 (100 Hz), 2 (200 Hz), 3 (250Hz), 4 (500Hz)
		[LED_txpwr]				- [7:5] x x x
											- [4:2] period of infrared LEDs (in multiples of 20 ms)
											- [1:0] power: 0(-18 dBm), 1(-12 dBm), 2(-6 dBm), 3(0 dBm)
		[range xyz]				- range: 0 (2 g), 1 (4 g), 2 (8 g), 3 (16 g)
		[range gyr]				- range: 0 (250 dps), 1 (500 dps), 2 (1000 dps), 3 (2000 dps)
		[range mag]				- range: (not used)
		[id_chmask]				- [7:3] device id [12:8]
											- [2:0] channel mask: LSbit (acc), bit 1 (gyro), bit 2 (mag)
		[id_LSByte]				- device id low byte
 
  == USB -> INTERFACE ==
 (disable rx: 8 bytes)
 [0x61(1)][dummy(7)]
		[dummy]						- ignored
 
 == USB -> INTERFACE -> WEAR ==
 (stim 1: 8 bytes)
 [0x62(1)][stim id(1)][resolution(1)][count on(1)][count off(1)][repetitions(1)][current(1)][checksum(1)]
		[stim id]					- id of the current stimulation
		[resolution]			- resolution of the stim unit: 0 (1 ms), 1 (10 ms), 2 (100 ms), 3 (1000 ms)
		[count on]				- couter number of units at ON
											- minimum value is 5 if [resolution] is equal to 0
											- maximum value is 200 if [resolution] is equal to 1
		[count off]				- couter number of units at OFF
		[repetitions]			- number of repetitions
		[current]					- current in miliamps
		[checksum]				- uint8 sum off all bytes
		
	Note 1: The real values of the resolution are given by: (X + 1) / 32768 = time [s],
					where X is 32 (1 ms), 327 (10 ms), 3276 (100 ms) and 32767 (1 s)
	Note 2: If "resolution
*/

#define CMD_FIXED_LEN 8
#define DATA_16BITs_LEN 22

typedef struct {
	uint8_t cmd;
	uint8_t id;
	uint8_t counter;
	uint8_t metadata;
	uint16_t data_sensor1[3];
	uint16_t data_sensor2[3];
	uint16_t data_sensor3[3];
} data_16_bits_t;

typedef struct {
	uint8_t cmd;
	uint8_t freq;
	uint8_t led_txpwr;
	uint8_t range_xyz,range_gyr,range_mag;
	uint8_t id_chmask;
	uint8_t id_LSByte;
} wake_up_t;
#define CMD_WAKE_UP 0x60

/* Used only by the WEAR device */
typedef struct {
	uint8_t len;
	uint8_t freq;
	uint8_t led;
	uint8_t txpwr;
	uint8_t range_xyz,range_gyr,range_mag;
	uint16_t id;
	bool use_acc;
	bool use_gyr;
	bool use_mag;
	uint8_t rf_channel;
	uint8_t mbits;
} my_wake_up_t;

typedef enum {
	LED_off = 0,
	LED_on = 1,
} led_t;

typedef struct {
	uint8_t cmd;
	uint8_t dummy[7];
} disable_rx_t;
#define CMD_DISABLE_RX 0x61

typedef struct {
    uint8_t cmd;
    uint8_t id;
    uint8_t resolution;
    uint8_t count_on,count_off;
    uint8_t repetitions;
    uint8_t current;
    uint8_t checksum;
} stim1_t;
#define CMD_STIM_1 0x62
#define LEN_STIM_1 8

typedef struct {
    uint8_t cmd;
    uint8_t notused;
    uint8_t frames_on,frames_off;
    uint16_t repetitions;
    uint8_t current;
    uint8_t checksum;
} stim2_t;
#define CMD_STIM_2 0x63
#define LEN_STIM_2 8

typedef struct {
	uint8_t cmd;
	uint16_t ms_on;
	uint16_t ms_off;
	uint16_t repetitions;
	uint16_t current;
	uint8_t checksum;
} stim3_t;
#define CMD_STIM_3 0x64
#define LEN_STIM_3 10

#endif /* _PROTOCOL_H_ */