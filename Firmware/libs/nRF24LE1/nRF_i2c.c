#include "nRF_i2c.h"

#include <nrf24le1.h>
#include <stdint.h>
#include <stdbool.h>

/************************************************************************/
/* Timing                                                               */
/************************************************************************/
#define NOP_ P0DIR = P0DIR
#define WAIT_690ns	NOP_;NOP_;
#define WAIT_940ns	NOP_;NOP_;NOP_
#define WAIT_1190ns	NOP_;NOP_;NOP_;NOP_
#define WAIT_1690ns	NOP_;NOP_;NOP_;NOP_;NOP_
#define WAIT_2200ns	NOP_;NOP_;NOP_;NOP_;NOP_;NOP_;NOP_;NOP_

/*
 tBUF = 1.3us (min)
 tHD.STA = 0.6uS (min)
 tCLK = 1.25us (min)
 tSU.STO = 0.6us (min)
 tSU.STA = 0.6us (min)
*/
#define tBUF		WAIT_1690ns
#define tHDSTA	WAIT_690ns
#define tCLK		WAIT_940ns
#define tSUSTO	WAIT_690ns
#define tSUSTA	WAIT_690ns

/************************************************************************/
/* Low level funtions                                                   */
/************************************************************************/
static void start(void)
{
	tBUF;
	clear_SDA;
	tHDSTA;
	clear_SCL;
	//tCLK;
}

static void stop(void)
{
	clear_SCL;
	clear_SDA;
	tCLK;
	set_SCL;
	tSUSTO;
	set_SDA;
}

static void shift_byte(uint8_t byte)
{
	// Each cycle
	//   __
	//__|
	
	uint8_t i = 0;
	
	for (; i < 8; i++)
	{
		clear_SCL;
		if (byte & 0x80)
			set_SDA;
		else
			clear_SDA;
		
		byte = byte << 1;
		
		tCLK;
		set_SCL;
		tCLK;
	}
}

static bool get_sda(void)
{
	uint8_t sda;	
	
	clear_SCL;
	SDA2in;
	tCLK;
	set_SCL;
	tCLK;
	sda = P0;
	clear_SDA;
	clear_SCL;
	SDA2out;
	if (P0 & SDApin)
		return true;
	return false;
}

/************************************************************************/
/* User funtions                                                        */
/************************************************************************/
void nRF_i2c_init(void)
{
	/* SCL */
	P0DIR &= ~(1<<SCLpin);	// To out
	P0CON = 0x7 | SCLpin;		// In: buff OFF
	
	/* SDA */
	P0DIR &= ~(1<<SDApin);	// To out
	P0CON = 0x10 | SDApin;	// In: No pull up/down resistors
	
	set_SCL;
	set_SDA;
}

void nRF_i2c_wReg(i2c_dev_t* dev)
{
	start();
	
	shift_byte(dev->add & 0xFE);	
	get_sda();	
	
	shift_byte(dev->reg);
	get_sda();	
	
	shift_byte(dev->reg_val);	
	get_sda();	
	
	stop();
}

void nRF_i2c_wReg_nbytes(i2c_dev_t* dev, uint8_t nbytes)
{
	uint8_t i = 0;
	
	uint8_t r[2];
	
	r[0] = 0x10;
	r[1] = 0;
	
	start();
	
	shift_byte(dev->add & 0xFE);	
	get_sda();
	
	shift_byte(dev->reg);
	get_sda();	
	
	/*
	for (; i < nbytes; i++)
	{
		shift_byte(dev->buff[i]);	
		get_sda();
	}
	*/
	
	shift_byte(dev->buff[0]);	
	get_sda();
	
	shift_byte(dev->buff[1]);	
	get_sda();
	
	stop();
}


//void check_stim(void);

void nRF_i2c_rReg(i2c_dev_t* dev, uint8_t nbytes)
{
	uint8_t i,j,byte;
	
	if (nbytes > I2C_NBUF)
		return;
	
	start();
	
	shift_byte(dev->add & 0xFE);	
	get_sda();
	
	shift_byte(dev->reg);
	get_sda();	

	/* Repeat Start */
	clear_SCL;
	set_SDA;
	tCLK;
	set_SCL;
	tSUSTA;
	clear_SDA;
	tHDSTA;
	
	shift_byte(dev->add | 0x01);	
	get_sda();
	
	//check_stim();
		
	for (j = 0; j < nbytes; j++) {
		SDA2in;
		byte = 0;
		for (i = 0; i < 8; i++)
		{
			clear_SCL;
			WAIT_690ns;
			WAIT_940ns;
			set_SCL;
			//tCLK;
			if (P0 & (1<<SDApin))
				byte |= (1<<(7-i));			
		}
		
		//check_stim();
				
		clear_SCL;
		SDA2out;
		if (j == nbytes-1)
			set_SDA;
		else
			clear_SDA;
		
		tCLK;
		set_SCL;
		tCLK;
		clear_SCL;
		dev->buff[j] = byte;
	}
	dev->reg_val = byte;
	
	tCLK;
	stop();
	return;
}