#ifndef _NRF_UART_H_
#define _NRF_UART_H_

#include <stdint.h>		// uint8_t, int8_t, uint16_t, int16_t, uint32_t and int32_t
#include <reg24le1.h>	// Register, Pins and interrupt vectors
#include <intrins.h>	// _nop_ ();

/************************************************************************/
/* User                                                                 */
/************************************************************************/
#define UART_NBUF_TX 64

#define RTS_pin			4
#define RTS_portpin	P04

#define CTS_pin			2

/************************************************************************/
/* Prototypes                                                           */
/************************************************************************/
void nRF_uart_loop(void);
void nRF_uart_init(uint16_t baud_rate_gen);
void nRF_uart_xmit(const uint8_t *buff, uint16_t len);
void nRF_uart_rcv(uint8_t byte);

#define nRF_uart_enable		RTS_portpin = 0
#define nRF_uart_disable	RTS_portpin = 1

#endif /* _NRF_UART_H_ */
