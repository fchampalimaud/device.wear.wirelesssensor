#ifndef _NRF_UART_H_
#define _NRF_UART_H_

#include <stdint.h>

/************************************************************************/
/* User                                                                 */
/************************************************************************/
#define UART_NBUF_TX 64

#define RTS_pin			2
#define RTS_portpin	P02

/************************************************************************/
/* Prototypes                                                           */
/************************************************************************/
void nRF_uart_init(uint16_t baud_rate_gen);
void nRF_uart_xmit(const uint8_t *buff, uint16_t len);
void nRF_uart_rcv(uint8_t byte);

#define nRF_uart_enable		RTS_portpin = 0
#define nRF_uart_disable	RTS_portpin = 1

#endif /* _NRF_UART_H_ */
