#include "nRF_radio.h"

#include <stdint.h>
#include <stdbool.h>
#include <reg24le1.h>
#include "nRF_uart_with_flowcontrol.h"

static uint8_t nrf_radio_rReg(uint8_t reg);
static uint8_t nrf_radio_wReg(uint8_t reg, uint8_t value);
static uint8_t nrf_radio_rNop(void);
static void nrf_write_multibyte_reg(uint8_t reg, const uint8_t *pbuf, uint8_t length);


#define HAL_NRF_RX_PLOAD 12

void nRF_radio_init(
	uint8_t mode,
	uint8_t rfpower,
	uint8_t bitrate,
	uint8_t rfch)
{
	uint8_t temp;
	
	RFCKEN = 1U;		// Enable the radio clock (0xE8 RFCON)
	RF = 1U;				// Enable RF interrupt (0xB8 IEN1)
	
	temp = nrf_radio_rReg(CONFIG);
	if (mode == RADIO_RX_MODE)
		nrf_radio_wReg(CONFIG, temp | PRIM_RX);
																		// Radio as a primary receiver
	else
		nrf_radio_wReg(CONFIG, temp & ~(PRIM_RX));
																		// Radio as a primary emitter
	nrf_radio_wReg(EN_AA, 0x01);			// Enable Enhanced ShockBurst data pipe 0
	nrf_radio_wReg(EN_RXADDR, 0x01);	// Enable data pipe 0
	nrf_radio_wReg(SETUP_AW, 1);			// Set address width to 3 bytes
	nrf_radio_wReg(SETUP_RETR, 0x03);	// Auto Retransmit Delay = 250uS
																		// Retransmit Count = 3 (2 miliseconds for 3 retrans)
	nrf_radio_wReg(RF_CH, rfch);			// Set channel (f[MHz] = 2400 + RF_CH). Range: [0;125]
	nrf_radio_wReg(RF_SETUP, rfpower | bitrate);
																		// Configure radio tx power and data rate
	nrf_radio_wReg(DYNPD, 1);					// Enable Dynamic Payload Length pipe 0
	nrf_radio_wReg(FEATURE, EN_DPL | EN_ACK_PAY);
																		// Enable Dynamic Payload Length
																		// Enable Payload with ACK	
	//nrf_radio_wReg(RX_PW_P0 + PIPE_NUM, RX_BYTES);
																		// Set payload len on pipe 0	
	if (mode == RADIO_RX_MODE)
	{
		nRF_radio_power_mode(RADIO_POWER_UP);
																		// Power up radio
		CE_HIGH();											// Enable receiver
	}
}

void nRF_radio_to_TX(void)
{
	uint8_t temp;
	CE_LOW();
	temp = nrf_radio_rReg(CONFIG);
	nrf_radio_wReg(CONFIG, temp & ~(PRIM_RX));
}

void nRF_radio_to_RX(void)
{
	uint8_t temp;
	temp = nrf_radio_rReg(CONFIG);
	nrf_radio_wReg(CONFIG, temp | PRIM_RX);
	nRF_radio_power_mode(RADIO_POWER_UP);
	CE_HIGH();
}

void nRF_radio_set_channel(uint8_t rfch)
{
	/*
	if (rfch > 31)
		rfch = 31;
	
	nrf_radio_wReg(RF_CH, rfch << 2);
	*/
	nrf_radio_wReg(RF_CH, rfch);
}

void nRF_radio_set_data_rate(uint8_t bitrate)
{
	uint8_t temp;
	
	temp = nrf_radio_rReg(CONFIG);
	temp &= ~(0x28);	
	
	nrf_radio_wReg(RF_SETUP, temp | bitrate);
}

void nRF_radio_set_retrans_delay(uint8_t nRetransDelay)
{
	uint8_t temp;
	
	if (nRetransDelay > 15)
		nRetransDelay = 15;
	
	temp = nrf_radio_rReg(SETUP_RETR);
	temp &= ~(0xF0);
	
	nrf_radio_wReg(SETUP_RETR, temp | (nRetransDelay<<4));
}

void nRF_radio_set_retrans(uint8_t nRetrans)
{
	uint8_t temp;
	
	if (nRetrans > 15)
		nRetrans = 15;
	
	temp = nrf_radio_rReg(SETUP_RETR);
	temp &= ~(0x0F);
	
	nrf_radio_wReg(SETUP_RETR, temp | nRetrans);
}

void nRF_radio_remove_auto_ack(void)
{
	nrf_radio_wReg(EN_AA, 0);
}
	
void nRF_radio_power_mode(uint8_t power)
{
	uint8_t temp;
	
	temp = nrf_radio_rReg(CONFIG);
	if (power == RADIO_POWER_UP)
		temp |= PWR_UP;
	else
		temp &= ~(PWR_UP);
	nrf_radio_wReg(CONFIG, temp);
}

uint8_t nRF_radio_read_RPD(void)
{	
	return nrf_radio_rReg(CD) & 0x01;
}

uint8_t nRF_radio_read_ARC_CNT(void)
{	
	return nrf_radio_rReg(OBSERVE_TX) & 0x0F;
}

void nRF_radio_clear_RX(void) {
	nrf_radio_wReg(STATUS, RX_DR);
}

void nRF_radio_clear_TX(void) {
	nrf_radio_wReg(STATUS, TX_DS);
}

void nRF_radio_clear_MAX_RT(void) {
	nrf_radio_wReg(STATUS, MAX_RT);
}

bool nRF_radio_check_RX(void) {
	return (nrf_radio_rReg(STATUS) & RX_DR) ? true : false;
}

bool nRF_radio_check_TX(void) {
	return (nrf_radio_rReg(STATUS) & TX_DS) ? true : false;
}

bool nRF_radio_check_MAX_RT(void) {
	uint8_t reg;
	reg = nrf_radio_rReg(STATUS);
	if (reg & MAX_RT)
		return true;
	else
		return false;
	return (nrf_radio_rReg(STATUS) & MAX_RT) ? true : false;
}

void nRF_radio_xmit(const uint8_t *tx_pload, uint8_t length)
{
  nrf_write_multibyte_reg(W_TX_PAYLOAD, tx_pload, length);
}

void nRF_radio_ack(const uint8_t *tx_pload, uint8_t length)
{
  nrf_write_multibyte_reg(W_ACK_PAYLOAD, tx_pload, length);
}

uint8_t nRF_radio_rcv(uint8_t *rx_pload)
{
	uint8_t len, i;
	//uint8_t origin_pipe;
	uint8_t pointer = 0;
	
	//origin_pipe = (nrf_radio_rNop() & 0xE) >> 1;
	len = nrf_radio_rReg(R_RX_PL_WID);
	
	CSN_LOW();

  HAL_NRF_HW_SPI_WRITE(R_RX_PAYLOAD);
  while(HAL_NRF_HW_SPI_BUSY);
  rx_pload[0] = HAL_NRF_HW_SPI_READ();	
	
	for (i = 0; i < len; i++)
	{
		HAL_NRF_HW_SPI_WRITE(0);
		while(HAL_NRF_HW_SPI_BUSY);  
		rx_pload[pointer++] = HAL_NRF_HW_SPI_READ();
	}

	CSN_HIGH();

	//while(len--)
		//*(rx_pload + len_rtn - len) = nrf_radio_rNop();
	
	return len;
}

/************************************************************************/
/* Radio interrupt                                                      */
/************************************************************************/
NRF_ISR()
{
	uint8_t temp;
	uint8_t temp2;
	
	//nRF_uart_disable;
	
	temp = nrf_radio_rReg(STATUS);
	
	if (temp & RX_DR)
	{
		do
		{
			nRF_radio_int_RX_DataReady();
			nrf_radio_wReg(STATUS, RX_DR);
			
			temp2 = nrf_radio_rReg(FIFO_STATUS);
		} while((temp2 & RX_EMPTY) == 0);
	}
	
	if (temp & TX_DS)
	{
		nRF_radio_int_TX_DataSent();
		nrf_radio_wReg(STATUS, TX_DS);
	}
	
	if (temp & MAX_RT)
	{
		nRF_radio_int_TX_MaxRetrans();
		nrf_radio_wReg(STATUS, MAX_RT);
	}	
	
	//nRF_uart_enable;
}

/************************************************************************/
/* SPI to access radio                                                  */
/************************************************************************/
static uint8_t nrf_radio_rReg(uint8_t reg)
{
  uint8_t temp;

  CSN_LOW();

  HAL_NRF_HW_SPI_WRITE(reg);
  while(HAL_NRF_HW_SPI_BUSY);
  temp = HAL_NRF_HW_SPI_READ();

  HAL_NRF_HW_SPI_WRITE(0);
  while(HAL_NRF_HW_SPI_BUSY);
  temp = HAL_NRF_HW_SPI_READ();

  CSN_HIGH();

  return temp;
}

static uint8_t nrf_radio_wReg(uint8_t reg, uint8_t value)
{
  uint8_t retval;
  uint8_t volatile dummy;

  CSN_LOW();

  HAL_NRF_HW_SPI_WRITE((W_REGISTER + reg));
  while(HAL_NRF_HW_SPI_BUSY);
  retval = HAL_NRF_HW_SPI_READ();

  HAL_NRF_HW_SPI_WRITE(value);
  while(HAL_NRF_HW_SPI_BUSY);
  dummy = HAL_NRF_HW_SPI_READ();

  CSN_HIGH();

  return retval;
}

static uint8_t nrf_radio_rNop(void)
{
  uint8_t retval;

  CSN_LOW();	
  
	HAL_NRF_HW_SPI_WRITE(NOP);
	while(HAL_NRF_HW_SPI_BUSY);
	retval = HAL_NRF_HW_SPI_READ();
	
  CSN_HIGH();

  return retval;
}

#define NRF_WRITE_MULTIBYTE_REG_COMMON_BODY \
  do \
  { \
    next = *buf; \
    buf++; \
    while(HAL_NRF_HW_SPI_BUSY) {}  /* wait for byte transfer finished */ \
    dummy = HAL_NRF_HW_SPI_READ(); \
    HAL_NRF_HW_SPI_WRITE(next); \
  } while (--length);
/*lint -esym(550,dummy) symbol not accessed*/ \
/*lint -esym(438,dummy) last assigned value not used*/ \
/*lint -esym(838,dummy) previously assigned value not used*/ \
void nrf_write_multibyte_reg(uint8_t reg, const uint8_t *pbuf, uint8_t length)
{
  uint8_t memtype;
  uint8_t next;
  uint8_t volatile dummy;

  memtype = *(uint8_t*)(&pbuf);

  CSN_LOW();
  HAL_NRF_HW_SPI_WRITE(reg);

  if (memtype == 0x00U)
  {
    const uint8_t data *buf = (const uint8_t data *)pbuf;
    NRF_WRITE_MULTIBYTE_REG_COMMON_BODY
  }
  else if (memtype == 0x01U)
  {
    const uint8_t xdata *buf = (const uint8_t xdata *)pbuf;
    NRF_WRITE_MULTIBYTE_REG_COMMON_BODY
  }
  else if (memtype == 0xFEU)
  {
    const uint8_t pdata *buf = (const uint8_t pdata *)pbuf;
    NRF_WRITE_MULTIBYTE_REG_COMMON_BODY
  }
  else
  {
    const uint8_t *buf = (const uint8_t *)pbuf;
    NRF_WRITE_MULTIBYTE_REG_COMMON_BODY
  }

  while(HAL_NRF_HW_SPI_BUSY) {} /* wait for byte transfer finished */
  dummy = HAL_NRF_HW_SPI_READ();
  CSN_HIGH();
}