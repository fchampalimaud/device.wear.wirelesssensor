#include "nRF_uart_no_flowcontrol.h"

#include "nrf24le1.h"
#include <stdint.h>
#include <stdbool.h>
#include <reg24le1.h>

/************************************************************************/
/* Variables and buffers                                                */
/***************** *******************************************************/
static bool uart_halt;

static uint8_t tx[UART_NBUF_TX];

#if UART_NBUF_TX > 256
	static uint16_t tail;
	static uint16_t head;
#else
	static uint8_t tail;
	static uint8_t head;
#endif

/************************************************************************/
/* Functions                                                            */
/************************************************************************/
/*
 Init the uart
 baud_rate_gen = round(1024 - (2*16e6)/(64*baud_rate))
 usualy used:
 500K (1023), 250K (1022), 166K65 (1021), 125K (1020),
 57K6 (1015), 38K4 (1011), 19K2 (998), 9K6 (972)
 only 10 bits are used from "baud_rate_gen" 
 */
void nRF_uart_init(uint16_t baud_rate_gen)
{	
  tail = 0;
	head = 0;
	uart_halt = true;
	
	/* PACKAGE_4x4: */
		P0DIR |= (1<<6);
		P0DIR &= ~(1<<5);
	/* PACKAGE_5x5: */
		//P0DIR |= (1<<4);
		//P0DIR &= ~(1<<3);
	/* case PACKAGE_7x7: */
		//P1DIR |= (1<<1);
		//P1DIR &= ~(1<<0);
	
	P0DIR &= ~(1<<RTS_pin);				// To out (default: normal drive strength)

  ES0 = 0;                      // Disable UART0 interrupt while initializing
  REN0 = 1;                     // Enable receiver
  SM0 = 0;                      // Mode 1..
  SM1 = 1;                      // ..8 bit variable baud rate
  PCON |= 0x80;                 // SMOD = 1
  ADCON |= 0x80;                // Select internal baud rate generator
  S0RELL = (uint8_t)baud_rate_gen;
  S0RELH = (uint8_t)(baud_rate_gen >> 8);

  TI0 = 0;											// Reset TX flag
  ES0 = 1;                      // Enable UART0 interrupt
}

/*
 uart RX and TX interrupt
*/
UART0_ISR()
{
	if (RI0 == 1)
  {
    RI0 = 0;
    nRF_uart_rcv(S0BUF);
  }
	
	//return;
	
  if (TI0 == 1)
  {
    TI0 = 0;
    if (head != tail )
    {
      S0BUF = tx[tail++];
			if (tail == UART_NBUF_TX)
				tail = 0;
		}
		else
		{
			uart_halt = true;
		  tail = 0;
			head = 0;
		}
  }
}

/*
 uart XMIT function
*/
void nRF_uart_xmit(const uint8_t *buff, uint16_t len)
{
	uint16_t i = 0;	
	/*
	for (; i < len; i++)
	{
		S0BUF = buff[i];
		while(TI0 != 1);
		TI0 = 0;
	}
	
	return;
	*/
		
	
	if (head + len < UART_NBUF_TX)
	{
		for (; i < len; i++)
			tx[head++] = buff[i];
		if (head == UART_NBUF_TX)
			head = 0;
	}
	else
	{
		for (; i < len; i++)
		{
			tx[head++] = buff[i];
			if (head == UART_NBUF_TX)
				head = 0;
		}
	}
	
	if(uart_halt)
	{
		S0BUF = tx[tail++];
		if (tail == UART_NBUF_TX)
			tail = 0;
		uart_halt = false;
	}
}