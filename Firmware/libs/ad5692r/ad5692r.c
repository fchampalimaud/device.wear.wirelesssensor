#include "ad5692r.h"

#include <stdint.h>
#include <stdbool.h>
#include <reg24le1.h>
#include "..\nRF24LE1\nRF_i2c.h"

/*
bool ad5692r_exist(i2c_dev_t dev)
{
	return true;
}

void ad5692r_reset(i2c_dev_t dev)
{
	
}
*/

void ad5692r_write_ctrl_reg(i2c_dev_t dev, uint8_t ctrl_reg)
{
	dev.reg = WRITE_CTRL_REG;
	dev.buff[0] = ctrl_reg;
	dev.buff[1] = 0;
	nRF_i2c_wReg_nbytes(&dev, 2);
}

void ad5692r_write_dac(i2c_dev_t dev, uint16_t dac)
{
	dev.reg = WRITE_DAC;
	dev.buff[0] = (dac >> 8) & 0x00FF;
	dev.buff[1] = dac & 0x00FF;
	nRF_i2c_wReg_nbytes(&dev, 2);
}