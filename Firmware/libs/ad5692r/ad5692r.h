#ifndef _AD5692R_H_
#define _AD5692R_H_

#include <stdbool.h>
#include <stdint.h>
#include "..\nRF24LE1\nRF_i2c.h"

/************************************************************************/
/* Command Bytes                                                        */
/************************************************************************/
#define NOP							0x00
#define WRITE_INPUT_REG	0x10
#define UPDATE_DAC			0x20
#define WRITE_DAC				0x30
#define WRITE_CTRL_REG	0x40

/************************************************************************/
/* Register contents                                                    */
/************************************************************************/
#define B_CTRL_MSByte_RESET			0x80

#define GM_CTRL_NORMAL_MODE			0x00
#define GM_CTRL_PWRDWN_1Kohm		0x20
#define GM_CTRL_PWRDWN_100Kohm	0x40
#define GM_CTRL_PWRDWN_3state		0x60

#define B_CTRL_REF							0x10

#define B_CTRL_GAIN							0x08


/************************************************************************/
/* Prototypes                                                           */
/************************************************************************/
//bool ad5692r_exist(i2c_dev_t dev);
//void ad5692r_reset(i2c_dev_t dev);
void ad5692r_write_ctrl_reg(i2c_dev_t dev, uint8_t ctrl_reg);
void ad5692r_write_dac(i2c_dev_t dev, uint16_t dac);
	
#endif /* _AD5692R_H_ */