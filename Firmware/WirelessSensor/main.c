/*
 1: Projects -> Options for file '...'... -> C51 -> Include Paths
 2: Include Paths equal to: C:\Nordic Semiconductor\nRFgo SDK 2.3.0.10040\source_code\hal;C:\Nordic Semiconductor\nRFgo SDK 2.3.0.10040\source_code\hal\nrf24le1;C:\Nordic Semiconductor\nRFgo SDK 2.3.0.10040\source_code\hal\nrf24l01p;C:\Nordic Semiconductor\nRFgo SDK 2.3.0.10040\source_code\compiler\common;C:\Nordic Semiconductor\nRFgo SDK 2.3.0.10040\source_code\compiler\c51
*/
#include <stdint.h>		// uint8_t, int8_t, uint16_t, int16_t, uint32_t and int32_t
#include <stdbool.h>	// bool, true and false
#include <reg24le1.h>	// Register, Pins and interrupt vectors
#include <intrins.h>	// _nop_ ();

/************************************************************************/
/* External sources                                                     */
/************************************************************************/
#include "..\libs\protocol\protocol.h"
#include "..\libs\mpu9150\mpu9150.h"
#include "..\libs\mpu9250\mpu9250.h"
#include "..\libs\ad5692r\ad5692r.h"
#include "..\libs\nRF24LE1\nRF_radio.h"
#include "..\libs\nRF24LE1\nRF_delays.h"
#include "..\libs\nRF24LE1\nRF_i2c.h"
#include "..\libs\nRF24LE1\nRF_watchdog.h"

/************************************************************************/
/* ID                                                                   */
/************************************************************************/
#define ID 2	// Range = [0;2^13-1] = [0;8191]

/************************************************************************/
/* Firmware version                                                     */
/************************************************************************/
#define FW_VERSION_MAJOR 4
#define FW_VERSION_MINOR 0

/************************************************************************/
/* Hardware version                                                     */
/************************************************************************/
#define HW_VERSION_MAJOR 2
#define HW_VERSION_MINOR 1

/************************************************************************/
/* Frequency center                                                     */
/************************************************************************/
#define CH_FREQ_START 43	// 2.443 GHz
// Note: 250Kbps reduces sensitivity on frequencies multiples of 16 (2400, 2416 and so on)

/************************************************************************/
/* Globals                                                              */
/************************************************************************/
data_16_bits_t data_16b;

#define WAKE_UP_SLEEP		2
#define WAKE_UP_STREAM	3
uint8_t xdata wake_up_state;
uint8_t use_stim_rtc_int = false;

my_wake_up_t xdata my_wake_up;

stim1_t stim1;
stim2_t stim2, my_stim2;
bool stim_LED_on;

#define BAT_HISTORY_MAX 10
uint8_t bat[BAT_HISTORY_MAX];
uint8_t bat_aux;
uint8_t bat_ptr = 0;
uint8_t bat_aux_counter;

uint8_t previous_bat = 100;

#define BAT_COUNTER 10	// 4.2s @ 50Hz, 2.1s @ 100Hz, 1.05s @ 200 Hz
uint8_t bat_counter = 0;

/************************************************************************/
/* Pins definition                                                      */
/************************************************************************/
#define IR_LEFT_pin				4
#define IR_LEFT_portpin		P04
#define IR_RIGHT_pin			5
#define IR_RIGHT_portpin	P05

#define STIM_pin					6
#define STIM_portpin			P06

#define set_IR_LEFT		IR_LEFT_portpin = 0
#define clr_IR_LEFT		IR_LEFT_portpin = 1
#define tgl_IR_LEFT		IR_LEFT_portpin = !IR_LEFT_portpin

#define set_IR_RIGHT	IR_RIGHT_portpin = 0
#define clr_IR_RIGHT	IR_RIGHT_portpin = 1
#define tgl_IR_RIGHT	IR_RIGHT_portpin = !IR_RIGHT_portpin

#define set_STIM			STIM_portpin = 1
#define clr_STIM			STIM_portpin = 0
#define tgl_STIM			STIM_portpin = !STIM_portpin

void pins_init(void)
{
	P0DIR &= ~(1<<IR_LEFT_pin);		// To out (default: normal drive strength)
	P0DIR &= ~(1<<IR_RIGHT_pin);	// To out (default: normal drive strength)	
	P0DIR &= ~(1<<STIM_pin);	// To out (default: normal drive strength)
	
	clr_IR_LEFT;
	clr_IR_RIGHT;
	set_STIM;
}

/************************************************************************/
/* CLK                                                                  */
/************************************************************************/
typedef enum
{
  CLK_XOSC16M_AND_RCOSC16M = 0,
  CLK_RCOSC16M = 1,
  CLK_XOSC16M = 2
} clk_source_t;

clk_source_t clk_get_16M_source(void)
{
  clk_source_t clock_source;
	
  if(CLKLFCTRL & (uint8_t)0x08U)
    clock_source = CLK_XOSC16M;
  else
    clock_source = CLK_RCOSC16M;

  return clock_source;
}

/************************************************************************/
/* main()                                                               */
/************************************************************************/
void error(void)
{	
	nRF_set_watchdog_ms(1);
	while(1);
}

i2c_dev_t motion;
i2c_dev_t dac;

uint8_t current = 10;

void yehh(void);
void main()
{		
	/* Configure clock for MCU (Cclk) */
	//CLKCTRL = CLK_XOSC16M << 4;									// XOSC16M not keeped on in Register Retention mode
																							// start only the XOSC16M
																							// frequency equal to 16 MHz
	//CLKCTRL = 0x80;
	CLKCTRL = 0x00;
	while(clk_get_16M_source() != CLK_XOSC16M);	// wait until XOSC16M is active/running
	
	/* Check wake up sequence */
	if (wake_up_state != WAKE_UP_SLEEP && wake_up_state != WAKE_UP_STREAM)
	{
		wake_up_state = WAKE_UP_SLEEP;
	}
		
		//wake_up_state = WAKE_UP_STREAM;
	
		/*
		my_wake_up.freq = MPU1950_FREQ_100Hz;
		my_wake_up.led = LED_off;
		my_wake_up.txpwr = RADIO_RFPWR_0dBm;
		my_wake_up.range_xyz = MPU1950_RANGE_ACC_4g;
		my_wake_up.range_gyr = MPU1950_RANGE_GYR_1000dps;
		my_wake_up.range_mag = MPU1950_RANGE_MAG_undifined;
		my_wake_up.chmask = MPU1950_ACC_on | MPU1950_GYR_on | MPU1950_MAG_on;
		*/
	
		//aux_wait_ms(5000);

	
	
	/* Configure clock for RTC2 and watchdog (CLKLF) */
	CLKLFCTRL = 0x01;														// select RCOSC32K
	while(!CLKLFCTRL & 0x40);										// wait until CLKLF is ready to use
	while(CLKLFCTRL & 0x80);										// make sure CLKLF goes to 1
	while(!CLKLFCTRL & 0x80);										// make sure CLKLF return to 0
	
	/* Select wake up sequence */
	if (wake_up_state == WAKE_UP_SLEEP)
	{	
		/* Configure TICK interrupt (it's the interrupt from RTC2) */
		/* (RTC2CMP + 1) / 32768 = time [s] */
		RTC2CMP1 = 0xFF;														// compare value MSByte (2 seconds)
		RTC2CMP0 = 0xFF;														// compare value LSByte
		RTC2CON = 0x07;															// compare mode 11 (timer counter resets)
																								// enable CLKLF to RTC2 core
		IEN1 |= 0x20;																// enable TICK interrupt
		
		/* Initialize radio */
		aux_wait_ms(500);
		nRF_radio_init(
			RADIO_RX_MODE,
			RADIO_RFPWR_0dBm,
			RADIO_BITRATE_2Mbps,
			10);
		
	}
	else
	{
		/*
		nRF_radio_init(
			RADIO_TX_MODE,
			RADIO_RFPWR_0dBm,
			RADIO_BITRATE_2Mbps,
			channel);
		*/
		//nRF_radio_power_mode(RADIO_POWER_UP);
	}
		
	/* Initialize pins */
	pins_init();
	
	/* Initialize motion sensor and I2C */
	mpu9150_init();
	nRF_i2c_init();
	motion.add = 0xD0;
	dac.add = 0x98;
	
	/* Configure DAC AD5692R to power down */
	ad5692r_write_ctrl_reg(dac, B_CTRL_REF | GM_CTRL_PWRDWN_100Kohm);
	set_STIM;
	my_stim2.repetitions = 0;
	
	/* Check if the MPU 9150 is there and force sleep */
	mpu9250_reset(motion); aux_wait_ms(10);
	mpu9250_reset(motion); aux_wait_ms(10);
	mpu9250_reset(motion); aux_wait_ms(10);
	mpu9250_reset(motion); aux_wait_ms(10);
	mpu9250_sleep(motion); aux_wait_ms(100);
	if (!mpu9250_exist(motion))
		error();
	
	if (wake_up_state == WAKE_UP_SLEEP)
	{
		/* Enable global interrupts (0xA8 IEN0) */
		EA = 1;
		
		CE_LOW();
		
		/* Enter Register Retention mode */	
		while(1)
		{
			PWRDWN = 0x04;														// Register retention mode
			PWRDWN = 0x07;														// Standby mode (wait for interrupt)
		}
	}
	else
	{			
		/* Go to sleep if watchdog resets the device */
		wake_up_state = WAKE_UP_SLEEP;
		
		/* Initialize (data) */
		data_16b.cmd = 0x00;
		data_16b.id = ID & 0x00FF;
		data_16b.counter = 0;
		data_16b.data_sensor1[0] = 0;
		data_16b.data_sensor1[1] = 0;
		data_16b.data_sensor1[2] = 0;
		data_16b.data_sensor2[0] = 0;
		data_16b.data_sensor2[1] = 0;
		data_16b.data_sensor2[2] = 0;
		data_16b.data_sensor3[0] = 0;
		data_16b.data_sensor3[1] = 0;
		data_16b.data_sensor3[2] = 0;
		
		/* Start MPU9150 */
		/*
		mpu9150_start(
			motion,
			my_wake_up.freq,
			my_wake_up.use_acc,
			my_wake_up.use_gyr,
			my_wake_up.use_mag,
			my_wake_up.range_xyz,
			my_wake_up.range_gyr,
			my_wake_up.range_mag);
		*/
		if (false == mpu9250_start(
			motion,
			my_wake_up.freq,
			my_wake_up.use_acc,
			my_wake_up.use_gyr,
			my_wake_up.use_mag,
			my_wake_up.range_xyz,
			my_wake_up.range_gyr * 10))
		{
			error();
		}
			
		/* Configure INT interrupt from MPU9150 */
		//INTEXP = 0x10;	// Configure interrupt from GP INT1 (Only one of the GP INTx may be set used at a time)
		//TCON = 0x01;		// External interrupt 0 type control. 1: falling edge, 0: low level
		//IEN0 |= 0x01;   // Enable Interrupt From Pin (IFP)
		
		/* Configure wake up pin */
		WUOPC0 = 0x04; 	// Wake up on pin P0.2 active-high (OPMCON)
		IEN1 = 0x08;		// Enable wake up on pin
		
		/* Configure INTERRUPT_TICK to Level 1 */
		IP0 = 0x20;
		
		/* Enable global interrupts (0xA8 IEN0) */
		EA = 1;
		
		/* Set watchdog reset to 20 seconds */
		nRF_set_watchdog_ms(20000);
		
		/* Enter Register Retention mode */	
		while(1)
		{
			PWRDWN = 0x04;														// Register retention mode
			PWRDWN = 0x07;														// Standby mode (wait for interrupt)
		}
		
		while(1);
	}
}


/************************************************************************/
/* WAKE_UP_SLEEP routine                                                */
/************************************************************************/
uint8_t _60us,_10us;

uint8_t wake_up_packet[34];
wake_up_t * p = wake_up_packet;
	
uint8_t mag_counter = 0;
uint8_t mag_div;

void stim_(void);
bool check_wake_up_old(void);
bool check_wake_up(void);
uint8_t read_bat_per(void);
	
void wakeup_tick() interrupt INTERRUPT_TICK
{
	uint8_t ch;
	uint8_t i, bat_percentage;
	
	if (use_stim_rtc_int)
	{
		stim_();
		return;
	}
	
	/* Timer was configured to one second */
	if (RTC2CMP1 == 0x7F)
	{		
		nRF_radio_init(
			RADIO_RX_MODE,
			RADIO_RFPWR_0dBm,
			RADIO_BITRATE_250Kbps,
			CH_FREQ_START + (ID%10) * 2);
		
		//for (_10us = 0; _10us < 40;_10us++)			// delay around 400 us
		//			aux_wait_10us();
		
		//for (ch = 0; ch < 32; ch++)
		//{			
			//nRF_radio_set_channel(ch);
			//nRF_radio_set_channel( CH_FREQ_START + (ID%10) * 2 );
			//nRF_radio_set_channel(1);
		
			/* _60us < 15 --> 816 us   */
			/* _60us < 20 --> 1.09 ms  */
			/* _60us < 36 --> 1.960 ms */
			/* _60us < 40 --> 2.180 ms */
			/* 250Kbps: 900 us (TX main.c line 388) * 2 + 150 us (start up time) + 130 us (RX Settling) + 100 us (margin) = 2180 us */
			/* It seems to be working well with 1.960 ms */
			for (_60us = 0; _60us < 40; _60us++)
			{
				for (_10us = 0; _10us < 4;_10us++)	// delay around 40 us
					aux_wait_10us();
				
				if (nRF_radio_check_RX())
				{
					my_wake_up.len = nRF_radio_rcv(wake_up_packet);					
					nRF_radio_clear_RX();
										
					my_wake_up.id = p->id_LSByte;
					my_wake_up.id |= (p->id_chmask & 0xF8) << 5;
					
					switch ((p->freq & 0x60) >> 5)
					{
						case 0:
							my_wake_up.mbits = RADIO_BITRATE_2Mbps;
							break;						
						case 1:
							my_wake_up.mbits = RADIO_BITRATE_1Mbps;
							break;						
						case 2:
							my_wake_up.mbits = RADIO_BITRATE_250Kbps;
							break;
					}
					
					p->freq &= 0x1F;
					
					bat_percentage = read_bat_per();
					bat_percentage = read_bat_per();
					for (i = 0; i < BAT_HISTORY_MAX; i++)
						bat[i] = bat_percentage;
					bat_ptr = 0;
					
					previous_bat = 100;
					
					if (my_wake_up.len == CMD_FIXED_LEN && my_wake_up.id == ID)
					{						
						//clr_STIM;
						//while(1);
						
						if (check_wake_up() == true)
						{							
							data_16b.counter = 0;
							
							wake_up_state = WAKE_UP_STREAM;
							
							nRF_set_watchdog_ms(10);
							while(1);
						}
					}						
				}				
			}
			
		CE_LOW();
	}
	
	/* Timer was configured to 2 second */
	if (RTC2CMP1 == 0xFF)
		/* Configure timer to 1 second */	
		RTC2CMP1 = 0x7F;
	else
		/* Configure timer to 2 seconds */	
		RTC2CMP1 = 0xFF;
}

bool check_wake_up_old(void)
{
			if (p->freq == 0)
			{			
				my_wake_up.freq = MPU1950_FREQ_50Hz;
				mag_div = 1;
			}
			else if (p->freq == 1)
			{
				my_wake_up.freq = MPU1950_FREQ_100Hz;
				mag_div = 2;
			}
			else if (p->freq == 2)
			{
				my_wake_up.freq = MPU1950_FREQ_200Hz;
				mag_div = 4;
			}
			else
				return false;
			
			if (((p->led_txpwr >> 2) & 0x3F) == 0)
				my_wake_up.led = LED_off;
			else if (((p->led_txpwr >> 2) & 0x3F) == 1)
				my_wake_up.led = LED_on;
			else
				return false;
		
			if ((p->led_txpwr & 0x03) == 0)
				my_wake_up.txpwr = RADIO_RFPWR_m18dBm;
			else if ((p->led_txpwr & 0x03) == 1)
				my_wake_up.txpwr = RADIO_RFPWR_m12dBm;
			else if ((p->led_txpwr & 0x03) == 2)
				my_wake_up.txpwr = RADIO_RFPWR_m6dBm;
			else if ((p->led_txpwr & 0x03) == 3)
				my_wake_up.txpwr = RADIO_RFPWR_0dBm;
			else
				return false;

			if (p->range_xyz == 0)
				my_wake_up.range_xyz = MPU1950_RANGE_ACC_2g;
			else if (p->range_xyz == 1)
				my_wake_up.range_xyz = MPU1950_RANGE_ACC_4g;
			else if (p->range_xyz == 2)
				my_wake_up.range_xyz = MPU1950_RANGE_ACC_8g;
			else if (p->range_xyz == 3)
				my_wake_up.range_xyz = MPU1950_RANGE_ACC_16g;
			else
				return false;

			if (p->range_gyr == 0)
				my_wake_up.range_gyr = MPU1950_RANGE_GYR_250dps;
			else if (p->range_gyr == 1)
				my_wake_up.range_gyr = MPU1950_RANGE_GYR_500dps;
			else if (p->range_gyr == 2)
				my_wake_up.range_gyr = MPU1950_RANGE_GYR_1000dps;
			else if (p->range_gyr == 3)
				my_wake_up.range_gyr = MPU1950_RANGE_GYR_2000dps;	
			else
				return false;
			
			my_wake_up.range_mag = p->range_mag;
			
			if ((p->id_chmask & 0x07) == 0)
				return false;
			my_wake_up.use_acc = (p->id_chmask & 0x01) ? true : false;
			my_wake_up.use_gyr = (p->id_chmask & 0x02) ? true : false;
			my_wake_up.use_mag = (p->id_chmask & 0x04) ? true : false;
			
			//my_wake_up.rf_channel = p->rf_channel;
			
			return true;
}

bool check_wake_up(void)
{
	mag_counter = 0;	
	if (p->freq == 0)
	{			
		my_wake_up.freq = 50;
		mag_div = 1;
	}
	else if (p->freq == 1)
	{
		my_wake_up.freq = 100;
		mag_div = 2;
	}
	else if (p->freq == 2)
	{
		my_wake_up.freq = 200;
		
		if (my_wake_up.mbits == RADIO_BITRATE_250Kbps)		
			mag_div = 2;	// 100 Hz
		else
			mag_div = 4;	// 50 Hz
	}
	else
		return false;
		
	my_wake_up.led = (p->led_txpwr >> 2) & 0x0F;

	if ((p->led_txpwr & 0x03) == 0)
		my_wake_up.txpwr = RADIO_RFPWR_m18dBm;
	else if ((p->led_txpwr & 0x03) == 1)
		my_wake_up.txpwr = RADIO_RFPWR_m12dBm;
	else if ((p->led_txpwr & 0x03) == 2)
		my_wake_up.txpwr = RADIO_RFPWR_m6dBm;
	else if ((p->led_txpwr & 0x03) == 3)
		my_wake_up.txpwr = RADIO_RFPWR_0dBm;
	else
		return false;

	if (p->range_xyz == 0)
		my_wake_up.range_xyz = 2;
	else if (p->range_xyz == 1)
		my_wake_up.range_xyz = 4;
	else if (p->range_xyz == 2)
		my_wake_up.range_xyz = 8;
	else if (p->range_xyz == 3)
		my_wake_up.range_xyz = 16;
	else
		return false;

	if (p->range_gyr == 0)
		my_wake_up.range_gyr = 25;	// It will be multiplied by 10
	else if (p->range_gyr == 1)
		my_wake_up.range_gyr = 50;
	else if (p->range_gyr == 2)
		my_wake_up.range_gyr = 100;
	else if (p->range_gyr == 3)
		my_wake_up.range_gyr = 200;	
	else
		return false;
	
	my_wake_up.range_mag = p->range_mag;
	
	if ((p->id_chmask & 0x07) == 0)
		return false;
	
	my_wake_up.use_acc = (p->id_chmask & 0x01) ? true : false;
	my_wake_up.use_gyr = (p->id_chmask & 0x02) ? true : false;
	my_wake_up.use_mag = (p->id_chmask & 0x04) ? true : false;

	/* Magnetometer can't work alone */
	if (my_wake_up.use_mag && !(my_wake_up.use_acc) && !(my_wake_up.use_gyr))
		return false;
	
	my_wake_up.rf_channel = CH_FREQ_START + (ID%10) * 2;			
	//my_wake_up.rf_channel = 1;
	
	return true;
}

/************************************************************************/
/* stimulation service routines                                         */
/************************************************************************/
uint16_t on_rtc2_cmp;
uint16_t off_rtc2_cmp;

uint8_t stim_on_counter, my_stim_on_counter;
uint8_t stim_off_counter, my_stim_off_counter;

uint8_t stim_repetitions, my_stim_repetitions;

/*
void check_stim(void)
{
	if (IRCON & 0x20)
	{
		stim_();
		IRCON &= ~(0x20);
	}
}
*/

void stim_(void)
{
	if (stim_LED_on)
	{		
		if (++my_stim_on_counter == stim_on_counter)
		{
			clr_STIM;
			
			RTC2CMP1 = (uint8_t)((off_rtc2_cmp >> 8) & 0x00FF);
			RTC2CMP0 = (uint8_t)((off_rtc2_cmp >> 0) & 0x00FF);
			
			my_stim_off_counter = 0;
			
			stim_LED_on = false;
			
			if (++my_stim_repetitions == stim_repetitions)
			{
				RTC2CON = 0x00;	// Resest RTC2
				IEN1 &= ~(0x20);
			}
			//my_stim_repetitions--;
		}
	}
	else
	{
		if (++my_stim_off_counter == stim_off_counter)
		{
			set_STIM;
			
			RTC2CMP1 = (uint8_t)((on_rtc2_cmp >> 8) & 0x00FF);
			RTC2CMP0 = (uint8_t)((on_rtc2_cmp >> 0) & 0x00FF);
			
			my_stim_on_counter = 0;
			
			stim_LED_on = true;
		}
	}
}

uint8_t stim_test_indicator = 0;

void stim2_(void)
{
	if (my_stim2.repetitions != 0)
	{
		if (my_stim2.frames_on != 0)
		{
			clr_STIM;
			stim_test_indicator = 1;
			my_stim2.frames_on--;
		}
		else
		{
			if (my_stim2.frames_off > 1)
			{
				set_STIM;
				stim_test_indicator = 0;
				my_stim2.frames_off--;
			}
			else
			{
				my_stim2.frames_on = stim2.frames_on;
				my_stim2.frames_off = stim2.frames_off;
				my_stim2.repetitions--;
				
				if (my_stim2.repetitions == 0)
				{
					set_STIM;
					stim_test_indicator = 0;
					ad5692r_write_ctrl_reg(dac, B_CTRL_REF | GM_CTRL_PWRDWN_100Kohm);
				}
			}
		}
	}
}

/************************************************************************/
/* radio service routines                                               */
/************************************************************************/
uint8_t radio_rx[36];

uint8_t stim_counter;

void nRF_radio_int_RX_DataReady(void)
{
	uint8_t i;
	
	nRF_radio_rcv(radio_rx);
	
	switch (radio_rx[0])
	{
		case CMD_STIM_1:
			stim1 = *((stim1_t*)(radio_rx));
			
			data_16b.cmd = 0x0C;		// send stim ID right away in the next packet
		
			if (stim1.resolution == 0)
				if (stim1.count_on < 5 || stim1.count_off < 5)
					break;
			if (stim1.resolution == 1)
				if (stim1.count_on > 200 || stim1.count_off > 200)
					break;
			
			RTC2CON = 0x00;	// Resest RTC2
			
			stim_repetitions = stim1.repetitions;
				
			my_stim_on_counter = 0;
			my_stim_off_counter = 0;
			my_stim_repetitions = 0;
				
			switch(stim1.resolution)
			{
				case 0:
					on_rtc2_cmp = 32 * stim1.count_on;
					off_rtc2_cmp = 32 * stim1.count_off;
					stim_on_counter = 1;
					stim_off_counter = 1;
					break;
				
				case 1:
					on_rtc2_cmp = 327 * stim1.count_on;
					off_rtc2_cmp = 327 * stim1.count_off;
					stim_on_counter = 1;
					stim_off_counter = 1;
					break;
				
				case 2:
					on_rtc2_cmp = 3276;
					off_rtc2_cmp = 3276;
					stim_on_counter = stim1.count_on;
					stim_off_counter = stim1.count_off;
					break;
				
				case 3:
					on_rtc2_cmp = 32767;
					off_rtc2_cmp = 32767;
					stim_on_counter = stim1.count_on;
					stim_off_counter = stim1.count_off;
					break;
			}
			
			RTC2CMP1 = (uint8_t)((on_rtc2_cmp >> 8) & 0x00FF);
			RTC2CMP0 = (uint8_t)((on_rtc2_cmp >> 0) & 0x00FF);
			RTC2CON = 0x07;
			IEN1 |= 0x20;
			
			set_STIM;
			stim_LED_on = true;
			
			use_stim_rtc_int = true;

			break;
		
		case CMD_STIM_2:
			stim2 = *((stim2_t*)(radio_rx));
			
			/* Copy the stim command to an usable variable */
			my_stim2 = stim2;
			
			/* Update from little-endian to big-endian */
			*(((uint8_t*)(&my_stim2.repetitions)) + 0) = *(((uint8_t*)(&stim2.repetitions)) + 1);
			*(((uint8_t*)(&my_stim2.repetitions)) + 1) = *(((uint8_t*)(&stim2.repetitions)) + 0);
			
			/* Check the checksum */
			my_stim2.checksum = 0;
			
      for (i = LEN_STIM_2 - 1; i != 0; i--)
				my_stim2.checksum += *(((uint8_t *)(&stim2)) + i - 1);	
			
			if (my_stim2.checksum != stim2.checksum)
				return;			
			
			/* Check the limits fof the current */
			if (my_stim2.current < 2)
				my_stim2.current = 2;
			if (my_stim2.current > 60)
				my_stim2.current = 60;
			
			/* Send stim counter in the next packet */
			//data_16b.cmd = 0x0C;
			//stim_counter = data_16b.counter - 1;
		
			/*
			Current measured:
			   DAC = 1024  -> 4.174 mA
			   DAC = 16384 -> 66.172 mA
	
			Current equation based on previous measurements:
			   m = (y1 - y0) / (x1 - x0) = (66.172 - 4.174) / (16384 - 1024) = 0.0040363
			   y = m*x + b <=> b = y - m*x <=> b = 1.174 - 0.0040363 * 1024 <=> b = 0.0408288
			   
			   y = 0.0040363 * x + 0.0408288
	
			Equation for device's stimlation circuitry:
			   DAC = current * 256 - 256 - 64
			*/	
			
			/* Disable STIM output and update DAC value */
			set_STIM;
			ad5692r_write_ctrl_reg(dac, GM_CTRL_NORMAL_MODE);
			ad5692r_write_dac(dac, ((uint16_t)my_stim2.current * 256) - 256 - 64);
			
			break;
		
		case CMD_WAKE_UP:
			break;
	}
}

uint8_t test = 0;
void nRF_radio_int_TX_DataSent(void)
{
	nRF_set_watchdog_ms(10000);
	/*
	test++;
	if (test == 50)
		clr_STIM;
	if (test == 100)
	{
		set_STIM;
		test = 0;
	}
	*/
	
}

void nRF_radio_int_TX_MaxRetrans(void)
{}

/************************************************************************/
/* WAKE_UP_STREAM routine                                               */
/************************************************************************/
void ir_led_ctrl(void);
void update_metadata(void);
	
uint8_t tx_fails = 0;
uint8_t led_cnt = 0;
	
//void ext0_irq(void) interrupt INTERRUPT_IPF
void wakeup_irq() interrupt INTERRUPT_WUOPIRQ
{
	PWRDWN = 0;
	//set_IR_LEFT;
		
	ir_led_ctrl();
	
	stim2_();
	
	if (my_wake_up.use_acc)
	{
		//check_stim();
		motion.reg = ACCEL_XOUT_H;
		nRF_i2c_rReg(&motion, 6);
		//check_stim();
		data_16b.data_sensor1[0] = motion.buff[1] | ((uint16_t)motion.buff[0] << 8);
		data_16b.data_sensor1[1] = motion.buff[3] | ((uint16_t)motion.buff[2] << 8);
		data_16b.data_sensor1[2] = motion.buff[5] | ((uint16_t)motion.buff[4] << 8);
		//check_stim();
	}	

	if (my_wake_up.use_gyr)
	{	
		//check_stim();
		motion.reg = GYRO_XOUT_H;
		nRF_i2c_rReg(&motion, 6);
		//check_stim();
		data_16b.data_sensor2[0] = motion.buff[1] | ((uint16_t)motion.buff[0] << 8);
		data_16b.data_sensor2[1] = motion.buff[3] | ((uint16_t)motion.buff[2] << 8);
		data_16b.data_sensor2[2] = motion.buff[5] | ((uint16_t)motion.buff[4] << 8);
		//check_stim();
	}
	
	if (my_wake_up.use_mag)
	{	
		//check_stim();
		if (++mag_counter == mag_div)
		{
			mag_counter = 0;
			mpu9150_mag_read_meas(motion, (uint8_t*)(&data_16b.data_sensor3[0]));
			mpu9150_mag_trig_single_meas(motion);
		}
		//check_stim();
		/*
		if (mag_counter == 0)
				mpu9150_mag_trig_single_meas(motion);
		*/
		//check_stim();
	}		
	
	tx_fails += nRF_radio_read_ARC_CNT();
	update_metadata();	
	
	//check_stim();
	
	nRF_radio_init(
		RADIO_TX_MODE,
		my_wake_up.txpwr,
		my_wake_up.mbits,
		my_wake_up.rf_channel);
	switch (my_wake_up.mbits)
	{
		case RADIO_BITRATE_2Mbps:
			nRF_radio_set_retrans_delay(0);	// 250us: 15 bytes max. on ACK payload
			
			if (my_wake_up.freq == 200)
				nRF_radio_set_retrans(3);
			else
				nRF_radio_set_retrans(4);
			
			break;
		case RADIO_BITRATE_1Mbps:
			nRF_radio_set_retrans_delay(1);	// 500us: any bytes on ACK payload
			
			if (my_wake_up.freq == 200)
				nRF_radio_set_retrans(2);
			else if (my_wake_up.freq == 100)
				nRF_radio_set_retrans(4);
			else
				nRF_radio_set_retrans(4);
			
			break;
		case RADIO_BITRATE_250Kbps:
			nRF_radio_set_retrans_delay(2);	// 750us: 8 bytes max. on ACK payload
			
			if (my_wake_up.freq == 200)
			{
				if ( (my_wake_up.use_acc) && (my_wake_up.use_gyr) && (my_wake_up.use_mag) )
					nRF_radio_set_retrans(0);
				else
					nRF_radio_set_retrans(1);
			}
			else if (my_wake_up.freq == 100)
				nRF_radio_set_retrans(3);
			else
				nRF_radio_set_retrans(5);
			
			break;
	}
	
	//check_stim();
	
	nRF_radio_power_mode(RADIO_POWER_UP);
	
	//for (_10us = 0; _10us < 1;_10us++)			// delay around 400 us
		//	aux_wait_10us();
	
	//check_stim();	
	
	if (stim_test_indicator)
		data_16b.counter |= 0x80;
	
	nRF_radio_xmit(((uint8_t*)(&data_16b)), DATA_16BITs_LEN);
	CE_PULSE();
	
	data_16b.counter &= ~(0x80);
	
	
	//clr_IR_LEFT;
	//check_stim();
	while (nRF_radio_check_TX() == false && nRF_radio_check_MAX_RT() == false);
	//check_stim();
	
	//nRF_radio_clear_TX();
	//nRF_radio_clear_MAX_RT();
	
	
	nRF_radio_power_mode(RADIO_POWER_DOWN);
	
	/* Control counter */
	if (++data_16b.counter & 0x80)
		data_16b.counter = 0;
	
	/* Control command */
	if (data_16b.cmd++ == 0x15)
		data_16b.cmd = 0;
	
	//CE_LOW();
	//IT1 = 1;			// (0x88 TCON)
}

void ir_led_ctrl(void)
{
	if (my_wake_up.led == 0)
		return;
	
	//check_stim();	

	if (led_cnt == ((my_wake_up.freq / 50) * 50))
		led_cnt = 0;
	
	if (led_cnt == 0)
	{
		set_IR_LEFT;
	}
	
	else if (led_cnt == (my_wake_up.led * (my_wake_up.freq / 50)))
	{
			clr_IR_LEFT;
			set_IR_RIGHT;
	}
	
	else if (led_cnt == ((my_wake_up.led * 2) * (my_wake_up.freq / 50)))
	{
		clr_IR_RIGHT;
	}
		
	led_cnt++;
}
			
uint8_t read_bat_per(void)
{
	uint8_t adc;
	
	ADCCON2 = 0x07;		// single ended, single conversion
										// 6 us power-down, 36 us acquitition duration
	ADCCON3 = 0x60;		// 8 bits, right justified
	ADCCON1 = 0x80;		// start conversion, power-up, AIN0, Internal 1.2 V			
	while (ADCCON1 & 0x40);

	// By characterization:
	// ADC = 182 when removed from charger
	// ADC = 155 around 5.5% of the time to shutdown
	//
	// per = (adc - 156) / 4
	
	// New values to acomodate LMS
	// ADC Max = 176
	// ADC Min = 143	
	adc = ADCDATL;
	
	if (adc >= 176)
		return 100;
	else if (adc <= 143)
		return 1;
	
	adc = adc - 143;
	adc = adc * 3;
	
	return adc;
}

void update_metadata(void)
{
	if (data_16b.cmd == 0x00)
	{
		data_16b.metadata = tx_fails;
		tx_fails = 0;
		return;
	}

	if (data_16b.cmd == 0x01)
	{
		data_16b.metadata = 129;
		return;
	}
	
	//check_stim();
	
	if (data_16b.cmd == 0x03)
	{
		motion.reg = TEMP_OUT_H;
		nRF_i2c_rReg(&motion, 1);
		data_16b.metadata = motion.reg_val;
		return;
	}	
	
	//check_stim();
	
	if (data_16b.cmd == 0x04)
	{
		if (my_wake_up.freq == 50)
			data_16b.metadata = 0;
		else if (my_wake_up.freq == 100)
			data_16b.metadata = 1;
		else if (my_wake_up.freq == 200)
			data_16b.metadata = 2;
		return;
	}
	
	if (data_16b.cmd == 0x05)
	{
		if (my_wake_up.txpwr == RADIO_RFPWR_m18dBm)
			data_16b.metadata = 0;
		else if (my_wake_up.txpwr == RADIO_RFPWR_m12dBm)
			data_16b.metadata = 1;
		else if (my_wake_up.txpwr == RADIO_RFPWR_m6dBm)
			data_16b.metadata = 2;
		else if (my_wake_up.txpwr == RADIO_RFPWR_0dBm)
			data_16b.metadata = 3;
		return;
	}
	
	//check_stim();
	
	if (data_16b.cmd == 0x06)
	{
		data_16b.metadata = my_wake_up.led;
		return;
	}
	
	if (data_16b.cmd == 0x07)
	{
		if (my_wake_up.range_xyz == 2)
			data_16b.metadata = 0;
		else if (my_wake_up.range_xyz == 4)
			data_16b.metadata = 1;
		else if (my_wake_up.range_xyz == 8)
			data_16b.metadata = 2;
		else if (my_wake_up.range_xyz == 16)
			data_16b.metadata = 3;
		return;
	}
	
	//check_stim();
	
	if (data_16b.cmd == 0x08)
	{
		if (my_wake_up.range_gyr == 25)
			data_16b.metadata = 0;
		else if (my_wake_up.range_gyr == 50)
			data_16b.metadata = 1;
		else if (my_wake_up.range_gyr == 100)
			data_16b.metadata = 2;
		else if (my_wake_up.range_gyr == 200)
			data_16b.metadata = 3;
		return;
	}

	if (data_16b.cmd == 0x09)
	{
		data_16b.metadata = my_wake_up.range_mag;
		return;
	}
	
	//check_stim();
	
	if (data_16b.cmd == 0x0A)
	{
		data_16b.metadata = 0;
		if (my_wake_up.use_acc)
			data_16b.metadata |= 0x01;
		if (my_wake_up.use_gyr)
			data_16b.metadata |= 0x02;
		if (my_wake_up.use_mag)
			data_16b.metadata |= 0x04;
		return;
	}
	
	//check_stim();

	if (data_16b.cmd == 0x0B)
	{		
		//if (++bat_counter == BAT_COUNTER)
		//{
			bat[bat_ptr] = read_bat_per();
			
			if (++bat_ptr == BAT_HISTORY_MAX)
				bat_ptr = 0;
			
			data_16b.metadata = 0;
			for (bat_aux_counter = 0; bat_aux_counter < BAT_HISTORY_MAX; bat_aux_counter++)
				if (bat[bat_aux_counter] > data_16b.metadata)
					data_16b.metadata = bat[bat_aux_counter];
			
			if (data_16b.metadata > previous_bat)
			{
				data_16b.metadata = previous_bat;
			}
			else
			{
				previous_bat = data_16b.metadata;
			}
				
			return;
		//}
		//else
		//{
		//	data_16b.cmd++;
		//}
	}
	
	//check_stim();
	
	if (data_16b.cmd == 0x0D)
	{		
		//if (bat_counter == BAT_COUNTER)
		//{
			//bat_counter = 0;
			
			data_16b.metadata = read_bat_per();
			
			return;
		//}
		//else
		//{
		//	data_16b.cmd++;
		//}
	}
	
	if (data_16b.cmd == 0x0C)
	{
		//data_16b.metadata = stim_counter;
		data_16b.cmd = 0x0E;
		//return;
	}
	
	if (data_16b.cmd == 0x0E)
	{
		data_16b.metadata = FW_VERSION_MAJOR;
		return;
	}
	
	if (data_16b.cmd == 0x0F)
	{
		data_16b.metadata = FW_VERSION_MINOR;
		return;
	}	
	
	if (data_16b.cmd == 0x10)
	{
		data_16b.metadata = HW_VERSION_MAJOR;
		return;
	}
	
	if (data_16b.cmd == 0x11)
	{
		data_16b.metadata = HW_VERSION_MINOR;
		return;
	}	
}



